package com.example.lpsil21.tdpreferences

import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.content.Context.MODE_PRIVATE
import android.content.SharedPreferences
import android.view.MenuItem
import android.widget.*
import com.example.lpsil21.tdpreferences.R.id.textView
import kotlin.reflect.KProperty
import android.widget.Toast
import android.widget.SeekBar




class UsersPreferences : AppCompatActivity() {

    companion object {
        val USERNAME = "USERNAME"
        var DEFAULT_USERNAME = "User"
        val SOUNDS = "SOUNDS"
        val DEFAULT_SOUNDS = 0
        val SPEED = "SPEED"
        val DEFAULT_SPEED = 1
    }

    var username by DelegatesExt.preference(this, USERNAME, DEFAULT_USERNAME)
    var sounds by DelegatesExt.preference(this, SOUNDS, DEFAULT_SOUNDS)
    var speed by DelegatesExt.preference(this, SPEED, DEFAULT_SPEED)


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.userpreferences)

        val button = findViewById(R.id.buttonSave) as Button
        val switch = findViewById(R.id.switch1) as Switch
        val seekBar = findViewById(R.id.seekBar2) as SeekBar
        seekBar.setProgress(speed)
        seekBar.refreshDrawableState()

        val user_input = findViewById(R.id.editText) as EditText

        //Preferences actuelles


        if(sounds == 1){
            switch.setChecked(true)
            switch.setOnCheckedChangeListener { buttonView, isChecked ->
                sounds = 0
            }
        }
        else if(sounds == 0){
            switch.setChecked(false)
            switch.setOnCheckedChangeListener { buttonView, isChecked ->
                sounds = 1
            }
        }

        seekBar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            internal var progressChangedValue = 0

            override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {
                progressChangedValue = progress
            }

            override fun onStartTrackingTouch(seekBar: SeekBar) {
                // TODO Auto-generated method stub
            }

            override fun onStopTrackingTouch(seekBar: SeekBar) {
                speed = progressChangedValue
            }
        })









        button.setOnClickListener {
            // Handler code here.
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
            username = user_input.text.toString()

            Toast.makeText(this@UsersPreferences, "Changes saved !",
                    Toast.LENGTH_SHORT).show()

        }




    }

}
