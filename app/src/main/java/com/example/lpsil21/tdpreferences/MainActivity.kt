package com.example.lpsil21.tdpreferences

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.content.SharedPreferences
import android.widget.Toast
import android.R.id.edit
import android.content.Context
import android.view.MenuItem
import android.widget.TextView
import com.example.lpsil21.tdpreferences.UsersPreferences.Companion.DEFAULT_SOUNDS
import com.example.lpsil21.tdpreferences.UsersPreferences.Companion.DEFAULT_USERNAME
import com.example.lpsil21.tdpreferences.UsersPreferences.Companion.SOUNDS
import com.example.lpsil21.tdpreferences.UsersPreferences.Companion.USERNAME


class MainActivity : AppCompatActivity() {



    var username by DelegatesExt.preference(this, USERNAME, DEFAULT_USERNAME)
    var sounds by DelegatesExt.preference(this, SOUNDS, DEFAULT_SOUNDS)
    var speed by DelegatesExt.preference(this, UsersPreferences.SPEED, UsersPreferences.DEFAULT_SPEED)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)



        var user_text = findViewById(R.id.textView2) as TextView
        var switch_text = findViewById(R.id.textView4) as TextView
        var speed_text = findViewById(R.id.textView) as TextView

        user_text.setText("Your username is " + username)
        if(sounds == 1){
            switch_text.setText("Sounds are enabled")
        }else if (sounds == 0){
            switch_text.setText("Sounds are disabled")
        }
        speed_text.setText("Your scrolling speed is " +speed.toString())


        var button = findViewById(R.id.button) as Button
        button.setOnClickListener {
            // Handler code here.
            val intent = Intent(this, UsersPreferences::class.java)
            startActivity(intent)

        }
    }



    /*override fun onBackPressed() {
        super.onBackPressed()
        username = user_text.text.toString().toLong().toString()
    }*/

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        android.R.id.home -> {
            onBackPressed()
            true
        }
        else -> false
    }













    /*val PREFS = "PREFS"
    val PREFS_USER = "PREFS_USER"
    val PREFS_SOUNDS = "PREFS_SOUNDS"
    val PREFS_SPEED = "PREFS_SPEED"
    lateinit var sharedPreferences: SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val button_pref = findViewById(R.id.button) as Button

        val user_text = findViewById(R.id.textView2) as TextView

        if (sharedPreferences.contains(PREFS_USER) && sharedPreferences.contains(PREFS_SOUNDS) && sharedPreferences.contains(PREFS_SPEED)) {

            val user = sharedPreferences.getString(PREFS_USER, "PREFS")
            val sounds = sharedPreferences.getInt(PREFS_SOUNDS, 0)
            val speed = sharedPreferences.getInt(PREFS_SPEED, 0)

            Toast.makeText(this, "User: $user sounds: $sounds speed: $speed", Toast.LENGTH_SHORT).show()

        }

        user_text.text = sharedPreferences.getString(PREFS_USER, null)


        button_pref.setOnClickListener {
            // Handler code here.
            val intent = Intent(this, UsersPreferences::class.java)
            startActivity(intent);
        }




    }*/
}
